from typing import List
from typing import Tuple
from typing import Dict
from typing import Union
from bs4 import BeautifulSoup
import requests
import time

BASE_URL = 'https://news.ycombinator.com/'


def get_news(url: str = BASE_URL, n_pages: int = 1, crawl_delay: int = 30) -> List[Dict[str, Union[int, str]]]:
    batch = []
    news = extract_news(url)
    batch.extend(news)
    n_pages -= 1
    print('Success')

    while n_pages > 0:
        try:
            time.sleep(crawl_delay)
            news, next_page_url = extract_next_page(url)
            url = BASE_URL + next_page_url
            batch.extend(news)
            n_pages -= 1
            print('Success')
        except BaseException:
            print('Failed')

    return batch


def extract_next_page(url: str) -> Tuple[List[Dict[str, Union[int, str]]], str]:
    r = requests.get(url)
    page = BeautifulSoup(r.text, 'html.parser')
    table = page.table.findAll('table')[1]
    tr = table.find_all('tr').pop()
    next_link = tr.find_all('td')[1].a['href']
    return (extract_news(BASE_URL + next_link), next_link)


def extract_news(url: str) -> List[Dict[str, Union[int, str]]]:
    print(f'Collecting data from page: {url} ...', end=' ')

    news = []

    r = requests.get(url)
    page = BeautifulSoup(r.text, 'html.parser')
    table = page.table.findAll('table')[1]

    trs = table.find_all('tr')

    del trs[2::3]

    trs.pop()
    trs.pop()

    tr = iter(trs)

    for i in range(len(trs)):
        try:

            tr1 = next(tr)

            title = tr1.find_all('td')[2].a.text

            href = tr1.find_all('td')[2].a['href']
            news_url = 'https://news.ycombinator.com/' + href if href.startswith('item?') else href

            tr2 = next(tr)

            author = tr2.find_all('td')[1].find_all('a')[0].text

            points_info = tr2.find_all('td')[1].span.text
            points = int(points_info[:len(points_info) - 6])

            comments_info = tr2.find_all('td')[1].find_all('a')[5].text
            if 'discuss' != comments_info:
                comments = int(comments_info[:len(comments_info) - 8])
            else:
                comments = 0

            record = {'author': author,
                      'comments': comments,
                      'points': points,
                      'title': title,
                      'url': news_url}

            news.append(record)

        except StopIteration:
            break

    return news
