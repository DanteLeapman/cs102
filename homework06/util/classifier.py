from util.preprocessing import parse_train_data
from util.preprocessing import get_stats
from util.preprocessing import clean
from util.auxiliary import db_fetch
from collections import defaultdict
from collections import namedtuple
from math import log
import random


def train_test_split(X, y, random_seed=None, train_size=0.7):
    random.seed(random_seed if random_seed != None else param.SEED)
    batch = list(zip(X, y))
    random.shuffle(batch)

    train = []
    for i in range(round(train_size * len(batch))):
        train.append(batch.pop())

    test = batch
    return ([X_train[0] for X_train in train],
            [y_train[1] for y_train in train],
            [X_test[0] for X_test in test],
            [y_test[1] for y_test in test])


class NaiveBayesClassifier:

    def __init__(self, alpha):
        self.smoothing_factor = alpha

    def fit(self, X, y):
        pool = parse_train_data(X)

        self.labels = list(set(news.label for news in X))
        self.train_data = namedtuple('Train', ['titles', 'authors', 'domains'])
        self.train_data.titles = get_stats(pool['titles'])
        self.train_data.authors = get_stats(pool['authors'])
        self.train_data.domains = get_stats(pool['domains'])

    def predict(self, X):
        prediction = []

        for news in X:

            tokens = clean(news.title)
            args = []

            for label in self.labels:

                logged_probabilities = []
                logged_probabilities.append(log(self.prior(label)))

                for token in tokens:
                    logged_probabilities.append(log(self.likelihood(token, label)))

                logged_probabilities.append(log(self.likelihood(news.author, label, mode='author')))

                domain = news.url.split('//')[-1].split('/')[0]
                logged_probabilities.append(log(self.likelihood(domain, label, mode='domain')))

                args.append((label, sum(logged_probabilities)))

            prediction.append((news, max(args, key=lambda x: x[1])))

        return prediction

    def score(self, X_test, y_test):
        total = 0
        correct = 0
        for i, result in enumerate(self.predict(X_test)):
            if int(result[1][0]) == int(y_test[i]):
                correct += 1
            total += 1
        return round(correct / total, 2)

    def nwords(self, def_dict, label=None):
        stat = defaultdict(int)

        for token in def_dict:
            for counts in def_dict[token].items():
                stat[str(counts[0])] += counts[1]

        return stat[label] if label != None else stat

    def likelihood(self, word, label, mode='title'):
        if mode == 'title':
            pool = self.train_data.titles
        elif mode == 'author':
            pool = self.train_data.authors
        elif mode == 'domain':
            pool = self.train_data.domains

        enc_of_this_word = pool[word][label] if pool[word] else 0
        all_words_in_class = self.nwords(pool, label)
        feature_vector_length = len(list(set(word for word in pool)))

        return (enc_of_this_word + self.smoothing_factor) / (all_words_in_class + self.smoothing_factor * feature_vector_length)

    def prior(self, label):
        data = db_fetch(labelled=True)
        batch = list(filter(lambda news: news.label == label, data))
        return len(batch) / len(data)
