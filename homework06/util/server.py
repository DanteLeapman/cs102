from util.classifier import NaiveBayesClassifier
from util.classifier import train_test_split
from util.scraping import BASE_URL
from util.scraping import get_news
from util.database import session
from util.database import News
from util.auxiliary import db_fetch
from bottle import route, run, request, template
from bottle import redirect
from sqlalchemy import exists
import util.parameters as param

NUM_PAGES = 1


def start_server():
    run(host='localhost', port=8080)


@route('/recommend')
def recommend_page():
    data = db_fetch(labelled=True)
    X, y = data, [news.label for news in data]
    X_train, y_train, X_test, y_test = train_test_split(X, y, param.SEED, train_size=param.TRAIN_SIZE)
    classifier = NaiveBayesClassifier(alpha=param.ALPHA)
    classifier.fit(X_train, y_train)

    data = []

    unlabeled = db_fetch(labelled=False)

    for record in classifier.predict(unlabeled):
        data.append((record[0], int(record[1][0]), record[1][1]))

    data = sorted(data, key=lambda x: (x[1], x[2]), reverse=True)

    return template("C:/1_cs102/homework06/templates/recommend.tpl", rows=data)


@route('/')
@route('/classify')
def classify_page():
    return template('C:/1_cs102/homework06/templates/classify.tpl', rows=db_fetch(labelled=False))


@route('/add_label')
def add_label():
    labels = {
        'good': 1,
        'unsure': 0,
        'bad': -1
    }

    try:
        label = labels[request.query.label] or labels['unsure']
        news_id = request.query.id

        s = session()
        news_piece = s.query(News).get(news_id)

        news_piece.label = label

        s.commit()
    except BaseException:
        pass

    redirect('/classify')


@route('/update_news')
def update_news():
    s = session()
    for piece in get_news(url=BASE_URL + 'newest', n_pages=NUM_PAGES, crawl_delay=30):
        news_piece = News(title=piece['title'],
                             author=piece['author'],
                             url=piece['url'],
                             comments=piece['comments'],
                             points=piece['points'])

        (piece_exists, ), = s.query(exists().where(News.title == piece['title'] and News.author == piece['author']))
        if not piece_exists:
            s.add(news_piece)

        s.commit()
    redirect('/classify')
