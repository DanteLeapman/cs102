from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk import word_tokenize
from collections import defaultdict
import util.parameters as param
import string


def clean(text):
    tokens = [token.lower() for token in word_tokenize(text)]
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    words = [word for word in stripped if word.isalpha()]

    if not param.PRESERVE_STOP_WORDS:
        stop_words = set(stopwords.words('english'))
        words = [w for w in words if w not in stop_words]

    if param.TOKEN_CLEANING_MODE == 'stemming':
        porter = PorterStemmer()
        cleaned = [porter.stem(word) for word in words]
    elif param.TOKEN_CLEANING_MODE == 'lemmatization':
        wordnet_lemmatizer = WordNetLemmatizer()
        cleaned = [wordnet_lemmatizer.lemmatize(word) for word in words]

    cleaned = list(set(cleaned))

    return cleaned


def get_stats(pool):
    stats = defaultdict(lambda: defaultdict(int))

    for entry in pool:
        stats[entry[1]][entry[0]] += 1

    return stats


def parse_train_data(X):
    pool = {'titles': [], 'authors': [], 'domains': []}

    for record in X:
        label, author, title, url = record.label, record.author, clean(record.title), record.url
        domain = url.split('//')[-1].split('/')[0]
        pool['titles'].extend([(label, word) for word in title])
        pool['authors'].extend([(label, author)])
        pool['domains'].extend([(label, domain)])

    return pool
