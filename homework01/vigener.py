def encrypt_vigenere(plaintext, keyword):
    """
    Encrypts plaintext using a Vigenere cipher.

    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    ciphertext = ''
    for index, abc in enumerate(plaintext):
        if 'a' <= abc <= 'z' or 'A' <= abc <= 'Z':
            shift = ord(keyword[index % len(keyword)])
            shift -= ord('a') if 'a' <= abc <= 'z' else ord('A')
            ans = ord(abc) + shift
            if 'a' <= abc <= 'z' and ans > ord('z'):
                ans -= 26
            elif 'A' <= abc <= 'Z' and ans > ord('Z'):
                ans -= 26
            ciphertext += chr(ans)
        else:
            chiphertext += abc
    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    """
    Decrypts a ciphertext using a Vigenere cipher.

    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    plaintext = ''
    for index, abc in enumerate(ciphertext):
        if 'a' <= abc <= 'z' or 'A' <= abc <= 'Z':
            shift = ord(keyword[index % len(keyword)])
            shift -= ord('a') if 'a' <= abc <= 'z' else ord('A')
            ans = ord(abc) - shift
            if 'a' <= abc <= 'z' and ans < ord('a'):
                ans += 26
            elif 'A' <= abc <= 'Z' and ans < ord('A'):
                ans += 26
            plaintext += chr(ans)
        else:
            plaintext += abc
    return plaintext
