def encrypt_caesar(plaintext):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("Python3.6")
    'Sbwkrq3.6'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ''
    for abc in plaintext:
        if ('a' <= abc <= 'z') or ('A' <= abc <= 'Z'):
            ans = ord(abc) + 3
            if (ans > ord('Z')) and (ans < ord('a')) or (ans > ord('z')):
                ans -= 26
            ciphertext += chr(ans)
        else:
            ciphertext += abc
    return ciphertext


def decrypt_caesar(ciphertext):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("Sbwkrq3.6")
    'Python3.6'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ''
    for abc in ciphertext:
        if ('a' <= abc <= 'z') or ('A' <= abc <= 'Z'):
            ans = ord(abc) - 3
            if (ans < ord('a')) and (ans > ord('Z')) or (ans < ord('A')):
                ans += 26
            plaintext += chr(ans)
        else:
            plaintext += abc
    return plaintext
